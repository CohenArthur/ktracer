# KTracer

## RayTracing Summer Project

This is a simple project that I'll do in June. The final build must meet the following specifications :

- Easy to use
- Performant
    - This means multi-threading support is required
- POSIX Compliant ?
- Well written, commented, and readable
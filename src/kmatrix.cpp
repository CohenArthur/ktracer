#include "kmatrix.h"

using namespace std;

KMatrix::KMatrix(){
    m_array = NULL;
}

KMatrix::KMatrix(int rows, int cols){
    this->rows = rows;
    this->cols = cols;

    int **matrix = new int * [this->rows];
    for (int index = 0; index < this->cols; index++)
        *(matrix + index) = new int[this->cols];

    m_array = matrix;
}

void KMatrix::error(string error_msg){
    cerr << "KMatrix : " << error_msg << endl;
    exit(1);
}

void KMatrix::insert(int value, int rows, int cols){
    if(rows >= this->rows || cols >= this->cols)
        error("Trying to insert value outside of bounds");
    m_array[rows][cols] = value;
}

int KMatrix::at(int rows, int cols){
    if(rows >= this->rows || cols >= this->cols)
        error("Trying to access value outside of bounds");
    return m_array[rows][cols];
}

void KMatrix::reset(int rows, int cols){
    this->rows = rows;
    this->cols = cols;

    int **matrix = new int * [this->rows];
    for (int index = 0; index < this->cols; index++)
        *(matrix + index) = new int[this->cols];

    m_array = matrix;
}

KMatrix::~KMatrix(){
    if (!m_array)
        error("Trying to free NULL matrix");
    for (int index = 0; index < this->cols; index++)
        delete [] m_array[index];
    delete [] m_array;
}

#include "kscene.h"

using namespace std;

KScene::KScene(){
    size_x = 0;
    size_y = 0;
    size_z = 0;
}

KScene::KScene(int size_x, int size_y, int size_z){
    this->size_x = size_x;
    this->size_y = size_y;
    this->size_z = size_z;

    KMatrix array[this->size_x];
    s_array = array;

    for(int x = 0; x < size_x; x++){
        s_array[x].reset(size_z, size_y);
    }
}

void KScene::error(string error_msg){
    cerr << "KScene : " << error_msg << endl;
    exit(1);
}

void KScene::insert(int value, int x, int y, int z){
    if (x >= size_x ||y >= size_y || z >= size_z)
        error("Trying to insert value out of bounds");
    s_array[x].insert(value, z, y);
}


int KScene::at(int x, int y, int z){
    if (x >= size_x ||y >= size_y || z >= size_z)
        error("Trying to access value out of bounds");
    return s_array[x].at(z, y);
}

KScene::~KScene(){
}

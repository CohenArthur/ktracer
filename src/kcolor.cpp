#include "kcolor.h"

KColor::KColor() {
}

KColor::KColor(uint8_t r, uint8_t g, uint8_t b) {
    this->r = r;
    this->g = g;
    this->b = b;
    grad = false;
}

KColor::KColor(uint8_t r, uint8_t g, uint8_t b, bool grad) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->grad = grad;
}

KColor::~KColor(){
}

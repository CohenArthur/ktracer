/*
 * A 'scene' is multiple matrices linked together, used to represent
 * a 3D environnement.
            ^
            | z
            |
            |
            |
            |
            |___________________________________> y
           /
          /
         /
        / x
*/

#pragma once

#include <iostream>
#include <string>

#include "kmatrix.h"

class KScene{
public:
    KScene();
    KScene(int size_x, int size_y, int size_z);

    void error(std::string error_msg);

    void insert(int value, int x, int y, int z);
    int at(int x, int y, int z);

    ~KScene();
private:
    int size_x;
    int size_y;
    int size_z;

    KMatrix *s_array;
};

#include "sdl_functions.h"

#define PI 3.14159265

using namespace std;

static inline
Uint8* pixel_ref(SDL_Surface *surface, unsigned x, unsigned y) {
	int bpp = surface->format->BytesPerPixel;
	return (Uint8*)surface->pixels + y * surface->pitch + x * bpp;
}

Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height) {
	Uint8 *p = pixel_ref(surface, width, height);

	switch(surface->format->BytesPerPixel){
		case 1:
			return *p;
		case 2:
			return *(Uint16 *)p;
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		case 4:
			return *(Uint32 *)p;
	}
		return 0;
}

void set_pixel(SDL_Surface *surface, unsigned height, unsigned width, Uint32 pixel) {
    Uint8 *p = pixel_ref(surface, height, width);

    switch(surface->format->BytesPerPixel){
        case 1:
            *p = pixel;
            break;
        case 2:
            *(Uint16 *)p = pixel;
            break;
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN){
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }else{
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;
        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void update_surface(SDL_Surface *screen, SDL_Surface *image) {
	if (SDL_BlitSurface(image, NULL, screen, NULL) < 0)
		warnx("BlitSurface error: %s Refreshing surface failed.\n", SDL_GetError());

	SDL_UpdateRect(screen, 0, 0, image->w, image->h);
}

void binarize(SDL_Surface *input) {
	size_t width	 = input->w;
	size_t height	 = input->h;

	for (size_t index_w = 0; index_w < width; index_w++) {
		for (size_t index_h = 0; index_h < height; index_h++) {

			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);

			if (r + g + b < 100) {
				r = 0;
				g = 0;
				b = 0;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			} else {
				r = 255;
				g = 255;
				b = 255;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			}
		}
	}
}

/*
void draw_rectangle(SDL_Surface *surface, int x, int y, int width, int height, KColor color){
    SDL_LockSurface(surface);

    if (x + width > surface->w || y + height > surface->h)
        exit(-1);

    Uint32 pixel = SDL_MapRGB(surface->format, color.r, color.g, color.b);

    for (int index_w = x; index_w < width; index_w++) {
        for (int index_h = y; index_h < height; index_h++) {
            set_pixel(surface, index_w, index_h, pixel);
        }
    }

    SDL_UnlockSurface(surface);
}

//FIXME
void draw_triangle(SDL_Surface *surface, int x, int y, int width, int height, KColor color){
    SDL_LockSurface(surface);

    Uint32 pixel = SDL_MapRGB(surface->format, color.r, color.g, color.b);

    for (int index_w = x; index_w < x + width; index_w++)
        for (int index_h = y; index_h < y + height; index_h++)
            if (index_h <= index_w)
                set_pixel(surface, index_w, index_h, pixel);

    SDL_UnlockSurface(surface);
}
	SDL_Rect rect_pos;
    rect_pos.x = x;
    rect_pos.y = y;

    SDL_BlitSurface(triangle, NULL, screen, &rect_pos);

    SDL_FreeSurface(triangle);
}
*/

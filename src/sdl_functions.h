#pragma once

#include <err.h>
#include <SDL/SDL.h>

//#include "SDL/SDL_rotozoom.h"

#include "kcolor.h"

/*
 * Returns the pixel at width * height on the SDL_Surface 'surface'
 */
Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height);

/*
 * Sets a pixel on 'surface' at the position width * height
 */
void set_pixel(SDL_Surface *surface, unsigned width, unsigned height, Uint32 pixel);

/*
 * Updates the surface, redraws it on the screen
 */
void update_surface(SDL_Surface *screen, SDL_Surface *image); //FIXME

/*
 * Binarizes the surface passed as arguments
 */
void binarize(SDL_Surface *input);

/*
 * Draws a rectangle of color 'color' between x, y, x + width and y + height
 */
//void draw_rectangle(SDL_Surface *surface, int x, int y, int width, int height, KColor color);

/*
 * Draws a triangle of color 'color' between x, y, x + width and y + height
 */
 //void draw_triangle(SDL_Surface *surface, int x, int y, int width, int height, KColor color);

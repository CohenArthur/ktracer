#include "kform.h"

using namespace std;

KForm::KForm(){
}

KForm::KForm(int type, int x, int y, int size_x, int size_y, KColor color){
    f_type = type;
    f_x = x;
    f_y = y;
    f_size_x = size_x;
    f_size_y = size_y;
    f_color = color;
    f_surface = NULL;
}

int KForm::get_type(){
    return f_type;
}

int KForm::get_x(){
    return f_x;
}

int KForm::get_y(){
    return f_y;
}

int KForm::get_size_x(){
    return f_size_x;
}

int KForm::get_size_y(){
    return f_size_y;
}

KColor KForm::get_color(){
    return f_color;
}

void KForm::set_surface(SDL_Surface *surface){
    f_surface = surface;
}

void KForm::draw(){
    switch(f_type){
        case K_SQUARE:
            draw_rectangle();
            break;
        case K_RECT:
            draw_rectangle();
            break;
        case K_TRIANGLE:
            draw_triangle();
            break;
        case K_CIRCLE:
            draw_circle();
            break;
    }
}

void KForm::error(string error_msg){
   cerr << RED "KForm Error : " RESET << error_msg << endl;
   exit(1);
}

KForm::~KForm(){
}

// Private functions
void KForm::sdl_error_check(){
    if (f_surface == NULL)
        error("Set the surface of the form before attempting to draw it");

    if (f_x + f_size_x > f_surface->w || f_y + f_size_y > f_surface->h)
        error("Coordinates are out of bounds");

}

void KForm::draw_rectangle(){
    sdl_error_check();

    SDL_LockSurface(f_surface); //Necessary ?

    Uint32 pixel = SDL_MapRGB(f_surface->format, f_color.r, f_color.g, f_color.b);

    for (int index_w = f_x; index_w < f_size_x; index_w++)
        for (int index_h = f_y; index_h < f_size_y; index_h++)
            set_pixel(f_surface, index_w, index_h, pixel);

    SDL_UnlockSurface(f_surface);
}

void KForm::draw_triangle(){
    sdl_error_check();

    SDL_LockSurface(f_surface); //Necessary ?

    Uint32 pixel = SDL_MapRGB(f_surface->format, f_color.r, f_color.g, f_color.b);

    for (int index_w = f_x; index_w < f_size_x; index_w++)
        for (int index_h = f_y; index_h < f_size_y; index_h++)
            if (index_h >= index_w)
                set_pixel(f_surface, index_w, index_h, pixel);

    SDL_UnlockSurface(f_surface);
}

void KForm::draw_circle(){
    sdl_error_check();

    SDL_LockSurface(f_surface);

    Uint32 pixel = SDL_MapRGB(f_surface->format, f_color.r, f_color.g, f_color.b);

    //While center != 0 maybe ?
    for (int r = -f_size_x; r < f_size_x; r++){
        int radius = (f_x + r) / 2;
        int x = (radius);
        int y = 0;
        int tx = 1;
        int ty = 1;
        int error = (tx - (radius * 2));

        while (x >= y){
            set_pixel(f_surface, f_x + x, f_y - y, pixel);
            set_pixel(f_surface, f_x + x, f_y + y, pixel);
            set_pixel(f_surface, f_x - x, f_y - y, pixel);
            set_pixel(f_surface, f_x - x, f_y + y, pixel);
            set_pixel(f_surface, f_x + y, f_y - x, pixel);
            set_pixel(f_surface, f_x + y, f_y + x, pixel);
            set_pixel(f_surface, f_x - y, f_y - x, pixel);
            set_pixel(f_surface, f_x - y, f_y + x, pixel);

            if (error <= 0){
                ++y;
                error += ty;
                ty += 2;
            } else {
                --x;
                tx += 2;
                error += (tx - (radius * 2));
            }
        }
    }

    SDL_UnlockSurface(f_surface);
}

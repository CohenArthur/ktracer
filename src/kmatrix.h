/*
 * Matrix implementation for the scenes.
 */

 #pragma once

 #include <iostream>
 #include <string>

class KMatrix{
public:
    KMatrix();
    KMatrix(int rows, int cols);

    void error(std::string error_msg);

    void insert(int value, int rows, int cols);
    int at(int rows, int cols);
    void reset(int rows, int cols);

    ~KMatrix();

    int rows;
    int cols;

private:
    int **m_array;
};

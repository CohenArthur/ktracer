/*
 * Simple 2D Form class. Switch to 3D incoming.
 */

#pragma once

#include <iostream>
#include <string>

#include "SDL/SDL.h"
#include "sdl_functions.h"

#include "kcolor.h"

#include "color.h"

#define K_SQUARE    0
#define K_RECT      1
#define K_TRIANGLE  2
#define K_CIRCLE    3

class KForm{
public:
    KForm();
    KForm(int type, int x, int y, int size_x, int size_y, KColor color);

//Getters for unit tests
    int get_type();
    int get_x();
    int get_y();
    int get_size_x();
    int get_size_y();
    KColor get_color();

//Sets the f_surface field. Necessary for drawing. Needs to be changed.
    void set_surface(SDL_Surface *surface);

//Draws the form onto f_surface. Change draw_...() functions
    void draw();

    /*
     * Simple error function : Outputs the message and exits(1)
     */
    void error(std::string error_msg);

    ~KForm();

private:
    std::string f_name;

    int f_type;
    int f_x;
    int f_y;
    int f_size_x;
    int f_size_y;

    KColor f_color;
    SDL_Surface *f_surface;

    void sdl_error_check();

    void draw_square(); //Necessary ?
    void draw_rectangle();
    void draw_triangle();
    void draw_circle();
};

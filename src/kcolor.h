/*
 * Basic color class used to represent a color.
 * Use Kcolor my_color(r_value, g_value, b_value) to create them
 */

#pragma once

#include <cstdint>

class KColor {
public:
    KColor();
    KColor(uint8_t r, uint8_t g, uint8_t b);
    KColor(uint8_t r, uint8_t g, uint8_t b, bool grad);

    ~KColor();

    uint8_t r, g, b;
    bool grad;
};

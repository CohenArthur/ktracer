#include <string>
#include <cstring>
#include <iostream>
#include <vector>
#include <unistd.h>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "src/tdd.h"

#include "src/sdl_functions.h"
#include "src/kcolor.h"
#include "src/kform.h"
#include "src/kmatrix.h"
#include "src/kscene.h"

#define VALUE_0 27
#define VALUE_1 729
#define VALUE_2 531441

using namespace std;

void pause_sdl(){
    bool run = true;
    SDL_Event event;

    while(run) {
        SDL_PollEvent(&event);
        switch(event.type){
            case SDL_QUIT:
            run = false;
        }
    }
}

void test_UnitTest(){
    UnitTest my_test("test_UnitTest()");

    my_test.progress(128);
    my_test.success();
    //my_test.alert(); //Exits the program
}

//FIXME
void test_kcolor(){
    UnitTest kcolor("test_kcolor()");

    KColor color(0, VALUE_0, 255);
    kcolor.assert_int(color.r, 0);
    kcolor.assert_int(color.g, VALUE_0);
    kcolor.assert_int(color.b, 255);
    kcolor.assert_bool(color.grad, false);

    KColor color_full(255, 0, VALUE_0, true);
    kcolor.assert_int(color_full.r, 255);
    kcolor.assert_int(color_full.g, 0);
    kcolor.assert_int(color_full.b, VALUE_0); //FIXME
    kcolor.assert_bool(color_full.grad, true);

    kcolor.success();
}

void test_kform(){
    UnitTest kform("test_kform()");

    KColor green(0, 255, 0);
    KForm square(K_SQUARE, 0, 0, 200, 200, green);

    kform.assert_int(square.get_type(), K_TRIANGLE); //FIXME

    kform.success();
}

void error_alloc(void *pointer){
    if (pointer == NULL) {
        cerr << "The pointer is NULL : Allocation failed." << endl;
        throw exception();
    }
}

void flip_wait(SDL_Surface *screen){
    SDL_Flip(screen);
    sleep(1);
}

void test_kform_draw(){
    UnitTest draw("test_kform_draw()");

    SDL_Init(SDL_INIT_VIDEO);
    SDL_WM_SetCaption("Processing...", NULL);

    SDL_Surface *screen = SDL_SetVideoMode(1280, 720, 32, SDL_HWSURFACE);
    error_alloc(screen);

    KColor bg(128, 128, 128);
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, bg.r, bg.g, bg.b));
//    flip_wait(screen);

    KColor white(255, 255, 255);
    KForm square(K_SQUARE, 0, 0, 200, 200, white);
    square.set_surface(screen);
    square.draw();
//    flip_wait(screen);

    KColor black(0, 0, 0);
    KForm rectangle(K_RECT, 100, 100, 200, 200, black);
    rectangle.set_surface(screen);
    rectangle.draw();
//    flip_wait(screen);

    KColor purple(255, 0, 255);
    KForm triangle(K_TRIANGLE, 200, 200, 500, 500, purple);
    triangle.set_surface(screen);
    triangle.draw();
//    flip_wait(screen);

    KColor orange(255, 128, 0);
    KForm circle(K_CIRCLE, 200, 200, 200, 0, orange);
    circle.set_surface(screen);
    circle.draw();

    SDL_Flip(screen);
    SDL_WM_SetCaption("KTracer", NULL);

    sleep(2);

    SDL_FreeSurface(screen);
    SDL_Quit();
}

void test_circle_draw(){
    SDL_Init(SDL_INIT_VIDEO);
    SDL_WM_SetCaption("Processing", NULL);
    SDL_Surface *screen = SDL_SetVideoMode(1280, 720, 32, SDL_HWSURFACE);
    error_alloc(screen);

	for(int index = 0; index < 150; index++){
		KColor color( index * 1.5, index * 1.5, 255);
		KForm circle(K_CIRCLE, index + 20, index + 20, 20, 20, color);

		circle.set_surface(screen);
		circle.draw();

		SDL_Flip(screen);
	}

    SDL_WM_SetCaption("KTracer", NULL);
    sleep(2);

    SDL_FreeSurface(screen);
    SDL_Quit();
}

void test_matrix_create(){
    UnitTest matrix_create("test_matrix_create");

    for (int index = 0; index < 1000; index++){
        KMatrix *my_mat = new KMatrix(120, 120);

        int index_r = 0, index_c = 0;
        for (index_r = 0; index_r < my_mat->rows; index_r++)
            for (index_c = 0; index_c < my_mat->cols; index_c++)
                my_mat->insert(index_r, index_r, index_c);

        for (index_r = 0; index_r < my_mat->rows; index_r++)
            for (index_c = 0; index_c < my_mat->cols; index_c++)
                matrix_create.assert_int(my_mat->at(index_r, index_c), index_r);

        delete my_mat; //Important
    }

    matrix_create.success();
}

void test_scene_create(){
    UnitTest scene_create("test_scene_create()");

    for(int index = 0; index < 1000; index++){
        KScene *test_scene = new KScene(index, index, index);

        delete test_scene;
    }

    scene_create.success();
}

void test_suite(){
/*
    test_UnitTest();
    test_kcolor();
    test_kform_draw();
    test_circle_draw();
    test_matrix_create();
*/
    test_scene_create();
}

void dev_test_suite(){
}

int main(void) {
    test_suite();

    dev_test_suite();
    return 0;
}

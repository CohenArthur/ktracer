CC=g++
CPPFLAGS= `pkg-config --cflags sdl`
CFLAGS= -Wall -Wextra -Werror -std=c99 -g

LDLIBS= `pkg-config --libs sdl`

SRC_DIR=./src/

ALL_C=$(wildcard $(SRC_DIR)*.c $(SRC_DIR)*.cpp)

SRC=ktracer.cpp ${ALL_C}
OBJ=${SRC:*.cpp = *.o}

TSRC=test.cpp ${ALL_C}
TOBJ=${TSRC:*.cpp=*.o}

all: ktracer test #Ajouter qrunch pour le rendu final.

ktracer : ${OBJ}

test : ${TOBJ}

clena: clean

clean:
	${RM} ktracer test *.o *.d
